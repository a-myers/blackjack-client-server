import socket
import sys
import time

if len(sys.argv) < 2:
    print("Specify the port as the first (and only) command line argument")
    exit()

port = int(sys.argv[1])
s = socket.socket()

s.connect(('127.0.0.1', port))

print("You have connected on port", port)

while True:
    msg = s.recv(1024).decode()
    msg = msg.split(';')
    cmd = msg[0]

    if(cmd == 'PRINT'):
        print(msg[1])
    elif(cmd == 'INPUT'):
        print(msg[1])
        s.send(input('>').encode())
    elif(cmd == 'INVALID'):
        print('ERROR:', msg[1])
