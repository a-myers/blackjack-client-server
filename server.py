import socket
import sys
import threading
import random
import time


class Deck():
    standard_deck = []

    def __init__(self, count):
        values = 'A23456789JQK'
        suits = 'HDSC'
        for value in values:
            for suit in suits:
                self.standard_deck.append(str(value) + str(suit))

        self.deck = self.standard_deck * count
        self.discard = []
        self.count = count

        random.shuffle(self.deck)
        print(self.deck)

    def shuffle(self):
        self.discard = []
        self.deck = self.standard_deck * self.count
        random.shuffle(self.deck)

    def draw(self):
        return self.deck.pop()


class Dealer(threading.Thread):
    def __init__(self, socket):
        threading.Thread.__init__(self)
        self.daemon = True
        self.socket = socket
        self.players = []
        self.active_players = []

    def broadcast(self, msg):
        for player in self.players:
            player.conn.send(('PRINT;' + msg).encode())
        time.sleep(1)

    def run(self):
        while True:
            if len(self.players) > 0:

                # Get Bets
                for player in self.players:
                    bet = player.getBet()
                    if(bet == 'leave'):
                        self.broadcast(player.name + ' has left the table')
                    else:
                        self.broadcast(player.name + ' has bet ' + str(bet))

                # Deal Cards

                # Play Turns

                # End Turn (pay out to winners)


class Player(threading.Thread):
    def __init__(self, address, conn, dealer):
        threading.Thread.__init__(self)
        self.daemon = True
        self.address = address
        self.conn = conn
        self.dealer = dealer
        self.hand = []
        self.bet = 0
        self.name = ''

    def send(self, msg):
        self.conn.send(msg.encode())
        time.sleep(1)

    def getInput(self, msg):
        self.conn.send(('INPUT;' + msg).encode())
        return self.conn.recv(1024).decode()

    def run(self):
        print('Connected to', self.address)
        self.name = self.getInput('please enter your name')
        self.dealer.players.append(self)
        self.dealer.broadcast(self.name + ' has joined the table')

    def getBet(self):
        response = self.getInput('Please place your bet:')
        if(response == 'leave'):
            self.dealer.broadcast('player has left the table')
            self.dealer.players.remove(self)
            print(self.dealer.players)
            return response
        else:
            try:
                bet = int(response)
                return bet
            except ValueError:
                self.conn.send(('INVALID;' + str(type(response))).encode())

    def playTurn(self):
        return

    def payOut(self):
        return


if len(sys.argv) < 2:
    print("Specify the port as the first (and only) command line argument")
    exit()

port = int(sys.argv[1])


# deck = Deck(2)


s = socket.socket()
s.bind(('', port))
s.listen(5)
print("Socket binded to port", port)


# Create Dealer Thread
dealer = Dealer(s)
dealer.start()


# Accept Player Threads
while True:
    conn, addr = s.accept()
    newThread = Player(addr, conn, dealer)
    newThread.start()
