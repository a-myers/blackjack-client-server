# blackjack-client-server

A client-server application that hosts the game blackjack. The server utilizes multithreading to handle multiple clients, and socket programming to maintain the connections.